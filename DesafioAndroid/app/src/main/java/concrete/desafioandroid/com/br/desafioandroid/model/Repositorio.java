package concrete.desafioandroid.com.br.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by unix on 01/02/18.
 */

public class Repositorio implements Serializable{

    @SerializedName("name")
    private String nome;

    @SerializedName("full_name")
    private String full_name;

    @SerializedName("description")
    private String descricao;

    @SerializedName("forks_count")
    private String forksCount;

    @SerializedName("stargazers_count")
    private String stargazersCount;

    @SerializedName("pulls_url")
    private String pullsRrl;

    @SerializedName("owner")
    private Owner owner;

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getForksCount() {
        return forksCount;
    }

    public void setForksCount(String forksCount) {
        this.forksCount = forksCount;
    }

    public String getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(String stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public String getPullsRrl() {
        return pullsRrl;
    }

    public void setPullsRrl(String pullsRrl) {
        this.pullsRrl = pullsRrl;
    }
}
