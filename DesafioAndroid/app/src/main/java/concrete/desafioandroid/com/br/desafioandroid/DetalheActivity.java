package concrete.desafioandroid.com.br.desafioandroid;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import concrete.desafioandroid.com.br.desafioandroid.adapter.DetalheAdapter;
import concrete.desafioandroid.com.br.desafioandroid.adapter.Onclick;
import concrete.desafioandroid.com.br.desafioandroid.factory.ViewModelFactory;
import concrete.desafioandroid.com.br.desafioandroid.model.Detalhe;
import concrete.desafioandroid.com.br.desafioandroid.model.DetalheViewModel;
import concrete.desafioandroid.com.br.desafioandroid.model.Repositorio;

/**
 * Created by unix on 06/02/18.
 */

public class DetalheActivity  extends AppCompatActivity implements Onclick{

    @BindView(R.id.my_toolbar_detalhe)
    protected Toolbar myToolbar;

    @BindView(R.id.recycler_detalhe)
    protected RecyclerView view;

    private DetalheAdapter detalheAdapter;
    private DetalheViewModel detalheViewModel;
    private Repositorio repositorio;
    private List<Detalhe> detalhes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalhe_repositorio);

        ButterKnife.bind(this);
        setSupportActionBar(myToolbar);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        view.setLayoutManager(llm);

        myToolbar.setTitleTextColor(getResources().getColor(R.color.colorwhite));

        detalheViewModel = ViewModelProviders.of(this,
                new ViewModelFactory(new DetalheViewModel(this))).get(DetalheViewModel.class);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle bundle = getIntent().getExtras();
        repositorio = (Repositorio) bundle.getSerializable("REPOSITORIO");

        detalheViewModel.getDetalhes(repositorio.getPullsRrl().replace("{/number}", "")).observe(this, new Observer<List<Detalhe>>() {
            @Override
            public void onChanged(@Nullable List<Detalhe> lista) {

                setDetalhes(lista);
                carregarDados();
            }
        });
    }

    public void carregarDados(){

        view.setAdapter(new DetalheAdapter(DetalheActivity.this, detalhes, this));
    }

    @Override
    public void onClick(int position) {

        Detalhe detalhe = detalhes.get(position);

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(detalhe.getUrl()));
        startActivity(i);

    }
    public List<Detalhe> getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(List<Detalhe> detalhes) {
        this.detalhes = detalhes;
    }
}
