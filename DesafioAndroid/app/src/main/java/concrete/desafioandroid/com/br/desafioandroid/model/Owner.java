package concrete.desafioandroid.com.br.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by unix on 01/02/18.
 */

public class Owner implements Serializable {

    @SerializedName("avatar_url")
    private String imagem;

    @SerializedName("login")
    private String login;

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
