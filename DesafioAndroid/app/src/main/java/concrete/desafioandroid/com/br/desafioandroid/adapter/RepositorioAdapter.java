package concrete.desafioandroid.com.br.desafioandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import concrete.desafioandroid.com.br.desafioandroid.R;
import concrete.desafioandroid.com.br.desafioandroid.model.Repositorio;
import concrete.desafioandroid.com.br.desafioandroid.viewholder.RepositorioHolder;

/**
 * Created by unix on 01/02/18.
 */

public class RepositorioAdapter extends RecyclerView.Adapter<RepositorioHolder> {

    private List<Repositorio> repositorios;
    private Context context;
    private OnItemClickListener clickListener;

    public RepositorioAdapter(Context context, List<Repositorio> repositorios, OnItemClickListener clickListener) {
        this.repositorios = repositorios;
        this.context = context;
        this.clickListener = clickListener;
    }

    @Override
    public RepositorioHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new RepositorioHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_repositorio, parent, false), clickListener);
    }

    @Override
    public void onBindViewHolder(RepositorioHolder holder, int position) {

        Repositorio repositorio = repositorios.get(position);

        holder.txvNomeRepositorio.setText(repositorio.getFull_name());
        holder.txvDescricaoRepositorio.setText(repositorio.getDescricao());
        holder.txvDescricaoRepositorio.setText(repositorio.getDescricao());
        holder.txvTotalForks.setText(repositorio.getForksCount());
        holder.txvTotalRings.setText(repositorio.getStargazersCount());
        holder.txvNome.setText(repositorio.getOwner().getLogin());
        holder.txvTotalRings.setText(repositorio.getStargazersCount());

        Picasso.with(context).load(repositorio.getOwner().getImagem()).into(holder.imageView);

    }


    @Override
    public int getItemCount() {
        return repositorios.size();
    }

    public List<Repositorio> getRepositorios() {
        return repositorios;
    }

    public void setRepositorios(List<Repositorio> repositorios) {
        this.repositorios = repositorios;
    }

}
