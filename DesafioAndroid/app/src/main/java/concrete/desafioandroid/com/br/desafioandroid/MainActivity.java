package concrete.desafioandroid.com.br.desafioandroid;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import concrete.desafioandroid.com.br.desafioandroid.adapter.OnItemClickListener;
import concrete.desafioandroid.com.br.desafioandroid.adapter.RepositorioAdapter;
import concrete.desafioandroid.com.br.desafioandroid.factory.ViewModelFactory;
import concrete.desafioandroid.com.br.desafioandroid.model.Repositorio;
import concrete.desafioandroid.com.br.desafioandroid.model.RepositorioViewModel;

public class MainActivity extends AppCompatActivity implements OnItemClickListener{

    @BindView(R.id.recycler)
    protected RecyclerView view;

    @BindView(R.id.my_toolbar)
    protected Toolbar myToolbar;

    @BindView(R.id.txt_page)
    protected TextView txtpage;

    private RepositorioViewModel repositorioViewModel;
    private RepositorioAdapter repositorioAdapter;
    private LinearLayoutManager layoutManager;
    private Repositorio repositorio;
    private int pages = 1;
    private int previousTotal = 0;
    private int oldPage = 0;
    private boolean loading = true;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private Bundle page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        myToolbar.setTitleTextColor(getResources().getColor(R.color.colorwhite));
        setSupportActionBar(myToolbar);
        page = new Bundle();
        savedInstanceState = page;
        txtpage.setText(String.valueOf(1));

        repositorioAdapter = new RepositorioAdapter(this, new ArrayList<Repositorio>(), this);
        layoutManager = new LinearLayoutManager(MainActivity.this);

        view.setLayoutManager(layoutManager);
        view.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView mRecyclerView, int dx, int dy) {
                super.onScrolled(mRecyclerView, dx, dy);

                totalItemCount = layoutManager.getItemCount();
                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                visibleItemCount = mRecyclerView.getChildCount();

                int totalSize = repositorioAdapter.getRepositorios().size() - 4;

                if((totalSize) == firstVisibleItem){

                    if ((firstVisibleItem + visibleItemCount) == totalItemCount) {

                        int i = ++pages;
                        carregarDados(i);
                    }
                }
            }

        });

        repositorioViewModel = ViewModelProviders.of(this,
                new ViewModelFactory(new RepositorioViewModel(this))).get(RepositorioViewModel.class);
    }

    @Override
    protected void onResume() {
        super.onResume();

        repositorioViewModel.getRepositorios().observe(this, new Observer<List<Repositorio>>() {
            @Override
            public void onChanged(@Nullable List<Repositorio> lista) {

                repositorioAdapter.getRepositorios().addAll(lista);

                view.setAdapter(repositorioAdapter);
            }
        });
    }

    private void carregarDados(int i){

        page.putInt("PAGE", i);
        repositorioViewModel.getRepositorio(i);
        txtpage.setText(String.valueOf(i));

        repositorioAdapter.getRepositorios().addAll((repositorioViewModel.getRepositorios().getValue()));

        view.setAdapter(repositorioAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("PAGE", pages);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        pages = savedInstanceState.getInt("PAGE");
        txtpage.setText(String.valueOf(pages));
    }

    @Override
    public void onClick(View view, int position) {

        repositorio = repositorioAdapter.getRepositorios().get(position);
        page.putSerializable("REPOSITORIO", repositorio);
        Intent intent = new Intent(this, DetalheActivity.class);
        intent.putExtras(page);
        startActivity(intent);
    }

    public Repositorio getRepositorio() {
        return repositorio;
    }

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }
}
