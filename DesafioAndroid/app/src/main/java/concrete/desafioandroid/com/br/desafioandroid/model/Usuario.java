package concrete.desafioandroid.com.br.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by unix on 06/02/18.
 */

public class Usuario implements Serializable {

    @SerializedName("login")
    private String nome;

    @SerializedName("avatar_url")
    private String foto;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
