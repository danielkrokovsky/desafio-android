package concrete.desafioandroid.com.br.desafioandroid.factory;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import concrete.desafioandroid.com.br.desafioandroid.model.DetalheViewModel;
import concrete.desafioandroid.com.br.desafioandroid.model.RepositorioViewModel;

/**
 * Created by unix on 02/02/18.
 */

public class ViewModelFactory implements ViewModelProvider.Factory {

    private ViewModel mViewModel;

    public ViewModelFactory(ViewModel viewModel) {
        this.mViewModel = viewModel;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(RepositorioViewModel.class)) {
            return (T) mViewModel;
        }
        if (modelClass.isAssignableFrom(DetalheViewModel.class)) {
            return (T) mViewModel;
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}
