package concrete.desafioandroid.com.br.desafioandroid.adapter;

import android.view.View;

/**
 * Created by unix on 06/02/18.
 */

public interface OnItemClickListener {

    public void onClick(View view, int position);
}