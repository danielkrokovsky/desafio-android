package concrete.desafioandroid.com.br.desafioandroid.util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by unix on 01/02/18.
 */

public class RetrofitInstance {

    private static String URL = "https://api.github.com";


    public static Retrofit getInstance() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

}
