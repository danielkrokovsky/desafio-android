package concrete.desafioandroid.com.br.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by unix on 01/02/18.
 */

public class RepositorioList implements Serializable{

    @SerializedName("items")
    private List<Repositorio> repositorio;

    public List<Repositorio> getRepositorio() {
        return repositorio;
    }

    public void setRepositorio(List<Repositorio> repositorio) {
        this.repositorio = repositorio;
    }
}
