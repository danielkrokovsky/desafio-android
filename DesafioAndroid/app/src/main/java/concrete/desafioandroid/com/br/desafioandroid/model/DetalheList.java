package concrete.desafioandroid.com.br.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by unix on 06/02/18.
 */

public class DetalheList implements Serializable {

    @SerializedName("items")
    private List<Detalhe> detalhes;

    public List<Detalhe> getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(List<Detalhe> detalhes) {
        this.detalhes = detalhes;
    }
}
