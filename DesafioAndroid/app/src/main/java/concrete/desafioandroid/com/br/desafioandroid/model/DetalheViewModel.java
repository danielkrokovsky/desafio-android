package concrete.desafioandroid.com.br.desafioandroid.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import java.util.List;

import concrete.desafioandroid.com.br.desafioandroid.DetalheActivity;
import concrete.desafioandroid.com.br.desafioandroid.services.GitHubService;
import concrete.desafioandroid.com.br.desafioandroid.util.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by unix on 06/02/18.
 */

public class DetalheViewModel extends ViewModel {

    private MutableLiveData<List<Detalhe>> detalhes;
    private GitHubService gitHubService;
    private DetalheActivity detalheActivity;


    public LiveData<List<Detalhe>> getDetalhes(String url) {

        if (detalhes == null) {
            detalhes = new MutableLiveData<List<Detalhe>>();

            getDetalhes2(url);
        }

        return detalhes;
    }

    public void setDetalhes(MutableLiveData<List<Detalhe>> detalhes) {
        this.detalhes = detalhes;
    }

    public DetalheViewModel(DetalheActivity detalheActivity) {

        gitHubService = RetrofitInstance.getInstance().create(GitHubService.class);
        this.detalheActivity = detalheActivity;
    }

    public void getDetalhes2(String url) {

        gitHubService.getUsuarioRepositorio(url).enqueue(new Callback<List<Detalhe>>() {
            @Override
            public void onResponse(Call<List<Detalhe>> call, Response<List<Detalhe>> response) {

                if(response.isSuccessful()){

                    detalhes.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Detalhe>> call, Throwable t) {

                Log.e("DANIEL", t.getMessage());
            }
        });

    }
}
