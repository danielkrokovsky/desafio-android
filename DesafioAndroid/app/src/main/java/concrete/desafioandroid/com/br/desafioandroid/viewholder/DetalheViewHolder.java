package concrete.desafioandroid.com.br.desafioandroid.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import concrete.desafioandroid.com.br.desafioandroid.R;
import concrete.desafioandroid.com.br.desafioandroid.adapter.Onclick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by unix on 06/02/18.
 */

public class DetalheViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    @BindView(R.id.txt_data)
    public TextView txtData;

    @BindView(R.id.txtNome)
    public TextView txtNome;

    @BindView(R.id.txtDescricao)
    public TextView txtDescricao;

    @BindView(R.id.txtTitulo)
    public TextView txtTitulo;

    @BindView(R.id.img_avatar)
    public CircleImageView imageView;

    private Onclick clickListener;

    public DetalheViewHolder(View itemView, Onclick clickListener) {
        super(itemView);

        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
        this.clickListener = clickListener;
    }


    @Override
    public void onClick(View v) {

        clickListener.onClick(getAdapterPosition());
    }
}
