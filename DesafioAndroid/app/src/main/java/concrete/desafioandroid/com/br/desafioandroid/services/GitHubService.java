package concrete.desafioandroid.com.br.desafioandroid.services;

import java.util.List;

import concrete.desafioandroid.com.br.desafioandroid.model.Detalhe;
import concrete.desafioandroid.com.br.desafioandroid.model.RepositorioList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by unix on 01/02/18.
 */

public interface GitHubService {

    @GET("/search/repositories?q=language:Java&sort=stars&page=page")
    Call<RepositorioList> getRepositorioData(@Query("page") int page);


    @GET()
    Call<List<Detalhe>> getUsuarioRepositorio(@Url String url);
}
