package concrete.desafioandroid.com.br.desafioandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import concrete.desafioandroid.com.br.desafioandroid.R;
import concrete.desafioandroid.com.br.desafioandroid.model.Detalhe;
import concrete.desafioandroid.com.br.desafioandroid.viewholder.DetalheViewHolder;

/**
 * Created by unix on 06/02/18.
 */

public class DetalheAdapter extends RecyclerView.Adapter<DetalheViewHolder> {

    private List<Detalhe> detalhes;
    private Context context;
    private Onclick clickListener;

    public DetalheAdapter(Context context, List<Detalhe> detalhes, Onclick clickListener) {
        this.detalhes = detalhes;
        this.context = context;
        this.clickListener = clickListener;
    }

    @Override
    public DetalheViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new DetalheViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_detalhe, parent, false), clickListener);
    }

    @Override
    public void onBindViewHolder(DetalheViewHolder holder, int position) {

        Detalhe detalhe = detalhes.get(position);

        holder.txtDescricao.setText(detalhe.getBody());
        holder.txtNome.setText(detalhe.getUsuario().getNome());
        holder.txtTitulo.setText(detalhe.getTitle());
        holder.txtData.setText(DetalheAdapter.formataData(detalhe.getDataPullRequest()));

        Picasso.with(context).load(detalhe.getUsuario().getFoto()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return detalhes.size();
    }

    public List<Detalhe> getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(List<Detalhe> detalhes) {
        this.detalhes = detalhes;
    }


    private static String formataData(String data) {

        String formattedTime = "";

        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date d = null;
            d = sdf.parse(data);

            SimpleDateFormat out = new SimpleDateFormat("dd/MM/yyyy");
            formattedTime = out.format(d);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formattedTime;
    }

}
