package concrete.desafioandroid.com.br.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by unix on 06/02/18.
 */

public class Detalhe implements Serializable {

    @SerializedName("url")
    private String url;

    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String body;

    @SerializedName("created_at")
    private String dataPullRequest;

    @SerializedName("user")
    private Usuario usuario;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDataPullRequest() {
        return dataPullRequest;
    }

    public void setDataPullRequest(String dataPullRequest) {
        this.dataPullRequest = dataPullRequest;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
