package concrete.desafioandroid.com.br.desafioandroid.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import concrete.desafioandroid.com.br.desafioandroid.R;
import concrete.desafioandroid.com.br.desafioandroid.adapter.OnItemClickListener;

/**
 * Created by unix on 01/02/18.
 */

public class RepositorioHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    @BindView(R.id.txvNomeRepositorio)
    public TextView txvNomeRepositorio;

    @BindView(R.id.txvDescricaoRepositorio)
    public TextView txvDescricaoRepositorio;

    @BindView(R.id.txvTotalForks)
    public TextView txvTotalForks;

    @BindView(R.id.txvTotalRings)
    public TextView txvTotalRings;

    @BindView(R.id.txvNome)
    public TextView txvNome;

    @BindView(R.id.imageView)
    public ImageView imageView;

    private OnItemClickListener clickListener;

    public RepositorioHolder(View itemView, OnItemClickListener clickListener) {
        super(itemView);

        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
        this.clickListener = clickListener;
    }


    @Override
    public void onClick(View v) {

        clickListener.onClick(v, getAdapterPosition());
    }
}
