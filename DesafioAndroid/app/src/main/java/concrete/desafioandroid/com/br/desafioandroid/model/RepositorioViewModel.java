package concrete.desafioandroid.com.br.desafioandroid.model;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import concrete.desafioandroid.com.br.desafioandroid.services.GitHubService;
import concrete.desafioandroid.com.br.desafioandroid.util.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by unix on 02/02/18.
 */

public class RepositorioViewModel extends ViewModel {

    private MutableLiveData<List<Repositorio>> repositorios;
    private GitHubService gitHubService;
    private Activity mainActivity;

    public LiveData<List<Repositorio>> getRepositorios() {
        if (repositorios == null) {
            repositorios = new MutableLiveData<List<Repositorio>>();

            getRepositorio(1);
        }
        return repositorios;
    }

    public RepositorioViewModel(Activity mainActivity) {

        gitHubService = RetrofitInstance.getInstance().create(GitHubService.class);
        this.mainActivity = mainActivity;
    }

    public void getRepositorio(int page) {

        gitHubService.getRepositorioData(page).enqueue(new Callback<RepositorioList>() {
            @Override
            public void onResponse(Call<RepositorioList> call, Response<RepositorioList> response) {

                if(response.isSuccessful()){

                    repositorios.setValue(response.body().getRepositorio());
                }
            }

            @Override
            public void onFailure(Call<RepositorioList> call, Throwable t) {

            }
        });
    }
}
